package conf

import "errors"

const (
	AddressEnv     string = "ADDRESS"
	GRPCAddressEnv string = "GRPC_ADDRESS"
)

var (
	ErrNoAddress     = errors.New("no address in configuration")
	ErrNoGRPCAddress = errors.New("no grpc address in configuration")
)

type Configuration struct {
	Address     string
	GRPCAddress string
}

func NewConfiguration(address string, grpcAddress string) (*Configuration, error) {
	if address == "" {
		return nil, ErrNoAddress
	}
	if grpcAddress == "" {
		return nil, ErrNoGRPCAddress
	}
	return &Configuration{
		Address:     address,
		GRPCAddress: grpcAddress,
	}, nil
}
