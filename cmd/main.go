package main

import (
	"os"

	"gitlab.com/test8930941/rest-server/cmd/app"
)

func main() {
	app := app.NewApplication()

	if err := app.Run(os.Args); err != nil {
		return
	}
}
