package app

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/urfave/cli/v2"
	"gitlab.com/test8930941/proto"
	"gitlab.com/test8930941/rest-server/cmd/conf"
	"gitlab.com/test8930941/rest-server/internal"
	"google.golang.org/grpc"
)

const (
	FlagAddress     = "address"
	FlagGRPCAddress = "grpc_address"
)

type Application struct {
	cli.App

	Configuration *conf.Configuration
}

func NewApplication() *Application {
	inst := &Application{}

	inst.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    FlagAddress,
			Aliases: []string{"a"},
			Usage:   "Define working address",
			EnvVars: []string{conf.AddressEnv},
		},
		&cli.StringFlag{
			Name:    FlagGRPCAddress,
			Aliases: []string{"grpc"},
			Usage:   "Define grpc connection address",
			EnvVars: []string{conf.GRPCAddressEnv},
		},
	}

	inst.Action = inst.run()

	inst.Before = func(c *cli.Context) error {
		config, err := conf.NewConfiguration(c.String(FlagAddress), c.String(FlagGRPCAddress))
		if err != nil {
			return err
		}

		inst.Configuration = config
		return nil
	}

	return inst
}

func (inst *Application) run() func(c *cli.Context) error {
	return func(c *cli.Context) error {
		grpcClient, err := grpc.Dial(inst.Configuration.GRPCAddress, grpc.WithInsecure())
		if err != nil {
			return err
		}
		defer grpcClient.Close()
		client := proto.NewUserServiceClient(grpcClient)

		service := internal.NewService(inst.Configuration.Address, client)

		go func() {
			if err := service.Run(); err != nil {
				log.Println("service error:", err.Error())
			}
		}()

		stopChan := make(chan os.Signal, 1)
		signal.Notify(stopChan, syscall.SIGINT, syscall.SIGTERM)
		<-stopChan

		service.Stop()

		// Пауза для graceful shutdown
		time.Sleep(2 * time.Second)
		return nil
	}
}
