package entities

import (
	"time"

	"github.com/gofrs/uuid"
)

type User struct {
	ID       uuid.UUID `json:"id"`
	Name     string    `json:"name"`
	Phone    string    `json:"phone"`
	Birthday time.Time `json:"birthday"`
}

type UserModification struct {
	Name     string    `json:"name"`
	Phone    string    `json:"phone"`
	Birthday time.Time `json:"birthday"`
}
type UserCreation struct {
	Name     string    `json:"name"`
	Phone    string    `json:"phone"`
	Birthday time.Time `json:"birthday"`
}
