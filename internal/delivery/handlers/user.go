package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"gitlab.com/test8930941/proto"
	"gitlab.com/test8930941/rest-server/internal/domain/entities"
	"gitlab.com/test8930941/rest-server/pkg/converters"
	"google.golang.org/protobuf/types/known/emptypb"
)

func GetUser(r *http.Request, conn proto.UserServiceClient) (interface{}, error) {
	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, errors.New("no id in request")
	}
	req := &proto.GetUserRequest{Id: id}
	ctx := context.Background()
	response, err := conn.GetUser(ctx, req)
	if err != nil {
		return nil, err
	}
	return converters.ConvertUserFromProto(response.GetUser())
}

func ListUsers(r *http.Request, conn proto.UserServiceClient) (interface{}, error) {
	ctx := context.Background()
	response, err := conn.GetUsersList(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}
	out := make([]*entities.User, 0, len(response.Users))
	for _, user := range response.Users {
		converted, err := converters.ConvertUserFromProto(user)
		if err != nil {
			return nil, err
		}
		out = append(out, converted)
	}
	return out, nil
}

func ModifyUser(r *http.Request, conn proto.UserServiceClient) (interface{}, error) {
	vars := mux.Vars(r)
	rawID, ok := vars["id"]
	if !ok {
		return nil, errors.New("no id in request")
	}
	id, err := uuid.FromString(rawID)
	if err != nil {
		return nil, errors.New("incorrect id")
	}
	req := entities.UserModification{}
	body, err := io.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(body, &req); err != nil {
		return nil, err
	}
	modifyRequest := &proto.ModifyUserRequest{User: &proto.User{
		Id:       id.String(),
		Name:     req.Name,
		Phone:    req.Phone,
		Birthday: req.Birthday.Format(converters.DBDateLayout),
	}}
	ctx := context.Background()
	modifyResponse, err := conn.ModifyUser(ctx, modifyRequest)
	if err != nil {
		return nil, err
	}
	return converters.ConvertUserFromProto(modifyResponse.GetUser())
}

func DeleteUser(r *http.Request, conn proto.UserServiceClient) (interface{}, error) {
	vars := mux.Vars(r)
	rawID, ok := vars["id"]
	if !ok {
		return nil, errors.New("no id in request")
	}
	id, err := uuid.FromString(rawID)
	if err != nil {
		return nil, errors.New("incorrect id")
	}
	req := &proto.DeleteUserRequest{Id: id.String()}
	ctx := context.Background()
	return conn.DeleteUser(ctx, req)
}

func CreateUser(r *http.Request, conn proto.UserServiceClient) (interface{}, error) {
	req := entities.UserCreation{}
	body, err := io.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(body, &req); err != nil {
		return nil, err
	}
	creationReq := &proto.CreateUserRequest{
		User: &proto.User{
			Name:     req.Name,
			Phone:    req.Phone,
			Birthday: req.Birthday.Format(converters.DBDateLayout),
		},
	}
	ctx := context.Background()

	resp, err := conn.CreateUser(ctx, creationReq)
	if err != nil {
		return nil, err
	}
	return converters.ConvertUserFromProto(resp.GetUser())
}
