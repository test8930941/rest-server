package internal

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/test8930941/proto"
	"gitlab.com/test8930941/rest-server/internal/delivery/handlers"
)

type Service struct {
	address string
	Server  *http.Server
	Client  proto.UserServiceClient
}

func NewService(address string, client proto.UserServiceClient) *Service {
	return &Service{address: address, Client: client}
}

func (inst *Service) Run() error {
	r := mux.NewRouter()
	s := r.PathPrefix("/user").Subrouter()
	s.HandleFunc("", inst.Do(handlers.ListUsers)).Methods("GET")
	s.HandleFunc("/{id}", inst.Do(handlers.GetUser)).Methods("GET")
	s.HandleFunc("", inst.Do(handlers.CreateUser)).Methods("PUT")
	s.HandleFunc("/{id}", inst.Do(handlers.ModifyUser)).Methods("POST")
	s.HandleFunc("/{id}", inst.Do(handlers.DeleteUser)).Methods("DELETE")
	srv := &http.Server{
		Addr:         inst.address,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}
	inst.Server = srv

	return inst.Server.ListenAndServe()
}

func (inst *Service) Stop() {
	inst.Server.Close()
}

func (inst *Service) Do(f HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if data, err := f(r, inst.Client); err != nil {
			response, err := json.Marshal(answer{Error: err.Error()})
			if err != nil {
				log.Println("marshal answer error:", err.Error())
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(response)
		} else {
			response, err := json.Marshal(answer{Result: data})
			if err != nil {
				log.Println("marshal answer error:", err.Error())
				return
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(response)
		}
	}
}

type answer struct {
	Result interface{} `json:"result"`
	Error  interface{} `json:"error"`
}

type HandlerFunc = func(*http.Request, proto.UserServiceClient) (interface{}, error)
