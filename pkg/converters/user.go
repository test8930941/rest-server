package converters

import (
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/test8930941/proto"
	"gitlab.com/test8930941/rest-server/internal/domain/entities"
)

const DBDateLayout = "2006-01-02 15:04:05"

func ConvertUserFromProto(proto *proto.User) (*entities.User, error) {
	id, err := uuid.FromString(proto.GetId())
	if err != nil {
		return nil, err
	}
	birthday, err := time.Parse(DBDateLayout, proto.GetBirthday())
	if err != nil {
		return nil, err
	}
	return &entities.User{
		ID:       id,
		Name:     proto.GetName(),
		Phone:    proto.GetPhone(),
		Birthday: birthday,
	}, nil
}
